# Contributing

Contributions are **welcome** and will be fully **credited**.

We accept contributions via Merge Requests on [GitLab](https://gitlab.com/theshopworks/git-review).

## Development
### Before You Start

Please follow the coding style:

- Follow [PHP-FIG PSR2 Coding Standards](https://www.php-fig.org/psr/psr-2/).
- Use 4 spaces over tabs.
- Use [PHP-CS-Fixer rules](https://github.com/FriendsOfPHP/PHP-CS-Fixer).
- Use [PHPStan](https://github.com/phpstan/phpstan) level 7, for static analysis of code.
- Use [PHPUnit](https://phpunit.de/) for automated tests.

### Git Hooks

We highly recommend using the fantastic Git Hooks tool called [Overcommit](https://github.com/brigade/overcommit), as part of any development contributions you may wish to carry out for this project.

> Git Hooks are based on a per-machine basis, so if you develop in different environments, you will have to install on all machines, to make use of this tool.
>
> Read more here: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks

If you have Ruby installed, it should just be a case of running:

```
gems install overcommit
cd ~/path/to/your/project

overcommit --install
```

If you don't have Ruby installed, here are some recommended steps for install:

### Ubuntu

```
cd $HOME
sudo apt-get update
sudo apt-get install autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev
```

### Fedora

```
yum install -y gcc-6 bzip2 openssl-devel libyaml-devel libffi-devel readline-devel zlib-devel gdbm-devel ncurses-devel
```

(This link explains why libssl-dev is installed: https://github.com/rbenv/ruby-build/wiki#missing-openssl)

```
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL

git clone https://github.com/rbenv/ruby-build.git
~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >>
~/.bashrc
exec $SHELL

rbenv install 2.5.1
rbenv global 2.5.1
ruby -v

gem install bundler
rbenv rehash

gems install overcommit
cd ~/path/to/your/project

overcommit --install
```

### Workflow

1. Fork [theshopworks/git-review](https://gitlab.com/theshopworks/git-review).
2. Clone the repository to your computer and install dependencies.

    {% code %}
    $ git clone https://gitlab.com/<username>/git-review.git
    $ cd git-review
    $ composer install
    {% endcode %}

3. Setup Git Hooks Overcommit tool, suggested above, to drastically improve development workflow.

4. Create a feature branch.

    {% code %}
    $ git checkout -b new-feature
    {% endcode %}

5. Start hacking.
6. Push the branch:

    {% code %}
    $ git push origin new-feature
    {% endcode %}

7. Create a merge request and describe the change.

## Merge Requests

- **Coding Syntax** - Please keep the code syntax consistent with the rest of the package.

- **Add unit tests!** - Your patch won't be accepted if it doesn't have tests.

- **Document any change in behavior** - Make sure the README and any other relevant documentation are kept up-to-date.

- **Use types over PhpDocBlocks** - Try to declare arguments types and return types, over using unnecessary PhpDocBlocks.

- **Consider our release cycle** - We try to follow [semver](http://semver.org/). Randomly breaking public APIs is not an option.

- **Create topic branches** - Don't ask us to pull from your master branch.

- **One pull request per feature** - If you want to do more than one thing, send multiple pull requests.

- **Send coherent history** - Make sure each individual commit in your pull request is meaningful.

### Updating Composer dependencies

As you can see, this project has specified other Composer vendor packages such as `friendsofphp/php-cs-fixer` and `squizlabs/php_codesniffer`, as `require` dependencies rather than `require-dev`. 
This allows for use of Git Review within a Docker container, offering better flexibility for local development and use in GitLab pipelines. Please be aware of this, when making any contributions involving Composer dependencies. Any updates to the `composer.lock` file should be examined during any code review process, for getting a contribution merged.