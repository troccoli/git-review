title: Git Review 0.1 Released
---
The initial version of this application has been released tagged version [v0.1](https://packagist.org/packages/theshopworks/git-review#v0.1). You can download this via Composer.

The initial version doesn't offer many features, and just contains the bare minimum to get the project going.
