title: Commands
---

## Code Style and Analysis

The following commands are simple wrappers for Open Source tools available from development communities, with the aim to hopefully speed
up development by only scanning what is necessary. These commands are ideal when developing on topic branches, as they should be smart
enough to only run checks or analysis, on **only** the changed files on the branch.

{% note info Git topic branches %}
A topic branch is a short-lived branch that you create and use for a single particular feature or related work.
These branches are opposite to Long-running branches, which are most likely named `master` and/or `develop`.

A usual workflow in Git is to merge short-lived topic branches into long-running branches, like `master`. Once the merge
has taken place, the topic branch is normally deleted.

https://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows
{% endnote %}

### es-lint

``` bash
$ ./git-review es-lint
```

Speed up ESLint checks by only running [ESLint](https://eslint.org/) checks on only the files that have changed on a branch,
when the current branch is not `master`.

If the current branch **is** `master`, then this command will run ESLint across all paths specified.

Example configuration:

``` yml git-review.yml.dist
tools:
  es_lint:  
    bin_path: "node_modules/.bin/eslint" 
    config_path: ".eslintrc.json" 
    extensions: 
      - "js" 
      - "jsx" 
    paths: 
      - "example/"
    exclude_paths:
      - "example/*/legacy"
```

Behind the scenes this will execute `node_modules/.bin/eslint --ext js --ext jsx example/` when the master branch is checkout out. 
If you are checked out on a topic branch and `example/file-has-been-changed.js` has been modified,
`node_modules/.bin/eslint --ext js --ext jsx example/file-has-changed.js` will execute.

### phpcs

``` bash
$ ./git-review phpcs
```

Speed up [PHP Code Sniffer](https://github.com/squizlabs/PHP_CodeSniffer) checks by only running checks on only the files that
have changed on a branch, when the current branch is not `master`.

If the current branch **is** `master`, then this command will run PHP Code Sniffer across all paths specified.

Example configuration:

``` yml git-review.yml.dist
tools:
  php_cs:  
    bin_path: "vendor/bin/phpcs"
    show_progress: true # options are true OR false
    verbosity_level: 1 #options are 0, 1, 2 and 3
    paths:
      - "app/"
      - "another_example/*/tests" #this allows for wildcard search
    exclude_paths:
      - "app/*/legacy" # used in combination with paths, app/* will be included but app/*/legacy should be excluded
```

Behind the scenes this will execute `php vendor/bin/phpcs app another_example/*/tests -p -v` when the master branch is checkout out. 
If you are checked out on a topic branch and `app/file-has-been-changed.php` is the only file on the branch that has changed,
then only `php vendor/bin/phpcs app/file-has-been-changed.php -p -v` will execute.

### phpcbf

``` bash
$ ./git-review phpcs
```

Speed up [PHP Code Beautifier (phpcbf)](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Fixing-Errors-Automatically) checks
by only running checks on only the files that have changed on a branch, when the current branch is not `master`.

If the current branch **is** `master`, then this command will run PHP Code Beautifier across all paths specified.

Example configuration:

``` yml git-review.yml.dist
tools:
  php_cbf:  
    bin_path: "vendor/bin/phpcbf"
    show_progress: true # options are true OR false
    verbosity_level: 1 #options are 0, 1, 2 and 3
    paths:
      - "app/"
      - "another_example/*/tests" #this allows for wildcard search 
    exclude_paths:
      - "app/*/legacy" # used in combination with paths, app/* will be included but app/*/legacy should be excluded
```

Behind the scenes this will execute `php vendor/bin/phpcbf app another_example/*/tests -p -v` when the master branch is checkout out. 
If you are checked out on a topic branch and `app/file-has-been-changed.php` is the only file on the branch that has changed, then only
`php vendor/bin/phpcbf app/file-has-been-changed.php -p -v` will execute.

### php-cs-fixer

``` bash
$ ./git-review php-cs-fixer
```

Speed up [PHP-CS-Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) checks by only running checks on only the files that have changed
on a branch, when the current branch is not `master`.

If the current branch **is** `master`, then this command will run PHP-CS-Fixer across all paths specified.

Example configuration:

``` yml git-review.yml.dist
tools:
  php_cs_fixer:  
    bin_path: "vendor/bin/php-cs-fixer" 
    config_path: ".php_cs.dist"
    verbosity_level: 1 #options are 0, 1, 2 and 3
    dry_run_mode: true # options are true OR false
    paths:
      - "app/"
      - "another_example/*/tests" #this allows for wildcard search 
    exclude_paths:
      - "app/*/legacy" # used in combination with paths, app/* will be included but app/*/legacy should be excluded
```

Behind the scenes this will execute `php vendor/bin/php-cs-fixer fix app another_example/*/tests --dry-run -v --config=.php_cs.dist`
when the master branch is checkout out.
If you are checked out on a topic branch and `app/file-has-been-changed.php` is the only file on the branch that has changed, then
only `php vendor/bin/php-cs-fixer fix app/file-has-been-changed.php --dry-run -v --config=.php_cs.dist` will execute.

### phpmd

``` bash
$ ./git-review phpmd
```

Speed up [PHP Mess Detector](https://phpmd.org/) checks by only running checks on only the files that have changed on a branch, when
the current branch is not `master`.

If the current branch **is** `master`, then this command will run PHP Mess Detector across all paths specified.

Example configuration:

``` yml git-review.yml.dist
tools:
  php_md:  
    bin_path: "vendor/bin/phpmd"
    report_format: "text" # text is used by default
    ruleset_file: "phpmd.xml" # "phpmd.xml" is used by default
    paths:
      - "app/"
      - "another_example/*/tests" #this allows for wildcard search
    exclude_paths:
      - "app/*/legacy" # used in combination with paths, app/* will be included but app/*/legacy should be excluded
```

Behind the scenes this will execute `php vendor/bin/phpmd app,another_example/*/tests text phpmd.xml` when the master branch is checkout out. 
If you are checked out on a topic branch and `app/file-has-been-changed.php` is the only file on the branch that has changed, then
only `php vendor/bin/phpmd app/file-has-been-changed.php text phpmd.xml` will execute.

### parallel-lint

``` bash
$ ./git-review parallel-lint
```

Speed up [PHP Parallel Lint](https://github.com/JakubOnderka/PHP-Parallel-Lint) checks by only running checks on only the files that
have changed on a branch, when the current branch is not `master`.

If the current branch **is** `master`, then this command will run PHP Parallel Lint across all paths specified.

Example configuration:

``` yml git-review.yml.dist
tools:
  php_parallel_lint:
    bin_path: "vendor/bin/parallel-lint"
    paths:
      - "app/"
      - "another_example/*/tests" #this allows for wildcard search
    exclude_paths:
      - "app/*/legacy" # used in combination with paths, app/* will be included but app/*/legacy should be excluded
```

Behind the scenes this will execute `php vendor/bin/parallel-lint app another_example/*/tests` when the master branch is checkout out. 
If you are checked out on a topic branch and `app/file-has-been-changed.php` is the only file on the branch that has changed, then
only `php vendor/bin/parallel-lint app/file-has-been-changed.php` will execute.

### phpstan

``` bash
$ ./git-review phpstan
```

Speed up [PHPStan](https://github.com/phpstan/phpstan) checks by only running checks on only the files that have changed on a branch,
when the current branch is not `master`.

If the current branch **is** `master`, then this command will run PHPStan across all paths specified.

Example configuration:

``` yml git-review.yml.dist
tools:
  php_stan:  
    bin_path: "vendor/bin/phpcbf"
    analysis_level: 7 # integer value ranging from 0 to 7
    config_path: "phpstan.neon" # this is used by default
    paths:
      - "app/"
      - "another_example/*/tests" #this allows for wildcard search
    exclude_paths:
      - "app/*/legacy" # used in combination with paths, app/* will be included but app/*/legacy should be excluded
```

Behind the scenes this will execute `php vendor/bin/phpstan analyse app another_example/*/tests -c phpstan.neon -l 7` when
the master branch is checkout out.
If you are checked out on a topic branch and `app/file-has-been-changed.php` is the only file on the branch that has changed, then
only `php vendor/bin/phpstan analyse app/file-has-been-changed.php -c phpstan.neon -l 7` will execute.

### phpunit

``` bash
$ ./git-review phpunit
```

Run PHPUnit on added or modified test files on a topic branch, only.

Option | Description
--- | ---
`--noResults` | Omit the final results of tasks output.

Consider the following example:

![](/git-review/images/phpunit-command.png)

We have modifed or added 12 tests files on a branch. When running `./git-review phpunit` when checked out on this branch,
it will scan only those files that have been changed. Currently, it scans each file one by one - so that you can view 
any potential errors, if any occur.

When checked out on `master`, the command will run `php vendor/bin/phpunit`, effectively running a full test suite. So
you will have to decide if this fits your development workflow when using this on `master`.

### task-sequence

``` bash
$ ./git-review task-sequence "console command 1" "console command etc"
```

Process several tasks in a sequential fashion and then receive results of all tasks in a single place.

This command is more of a helper utility command, rather than specific to topic branch files.

![](/git-review/images/task-sequence-command.png)
