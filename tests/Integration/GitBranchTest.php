<?php

declare(strict_types=1);

namespace Shopworks\Tests\Integration;

use Faker\Factory;
use Mockery;
use OndraM\CiDetector\Ci\CiInterface;
use OndraM\CiDetector\CiDetector;
use Shopworks\Git\Review\Commit\Commit;
use Shopworks\Git\Review\Commit\CommitLogParser;
use Shopworks\Git\Review\Commit\CommitParser;
use Shopworks\Git\Review\File\File;
use Shopworks\Git\Review\Process\Process;
use Shopworks\Git\Review\Process\Processor;
use Shopworks\Git\Review\Utility\CoreFunctions;
use Shopworks\Git\Review\VersionControl\GitBranch;
use Shopworks\Tests\GitTestCase;

class GitBranchTest extends GitTestCase
{
    /** @var GitBranch $gitBranch */
    private $gitBranch;
    private $topicBranchName;

    public function setUp(): void
    {
        parent::setUp();

        $this->topicBranchName = (Factory::create())->word;

        $command = <<<EOT
touch master-file-a.txt && /usr/bin/git add master-file-a.txt && /usr/bin/git commit -m "master commit a" &&
/usr/bin/git checkout -b {$this->topicBranchName} && touch test-branch-file-a.txt &&
/usr/bin/git add test-branch-file-a.txt &&
/usr/bin/git commit -m "test branch commit a" && touch test-branch-file-b.txt &&
/usr/bin/git add test-branch-file-b.txt && /usr/bin/git commit -m "test branch commit b" &&
/usr/bin/git checkout master && touch master-file-b.txt && /usr/bin/git add . &&
/usr/bin/git commit -m "master commit b"
EOT;

        $this->runProcess($command);
        $this->gitBranch = new GitBranch(
            Mockery::mock(CiDetector::class, [
                'isCiDetected' => false,
            ]),
            $this->directory,
            new Processor(),
            new CommitLogParser(new CoreFunctions()),
            new CommitParser(new Processor(), new Process(), $this->directory)
        );

        $this->assertEquals('master', $this->gitBranch->getName());
        $this->assertFalse($this->gitBranch->isDirty());
    }

    /**
     * @test
     */
    public function it_can_retrieve_branch_name(): void
    {
        $branchName = $this->gitBranch->getName();

        $this->assertEquals('master', $branchName);

        $this->checkoutBranch($this->topicBranchName);

        $branchName = $this->gitBranch->getName();

        $this->assertEquals($this->topicBranchName, $branchName);
    }

    /** @test */
    public function it_gets_the_branch_name_from_continuous_integration(): void
    {
        $gitBranch = new GitBranch(
            Mockery::mock(CiDetector::class, [
                'isCiDetected' => true,
                'detect' => Mockery::mock(CiInterface::class, [
                    'getGitBranch' => 'example-branch',
                ]),
            ]),
            $this->directory,
            new Processor(),
            new CommitLogParser(new CoreFunctions()),
            new CommitParser(new Processor(), new Process(), $this->directory)
        );

        $this->assertEquals('remotes/origin/example-branch', $gitBranch->getName());
    }

    /**
     * @test
     */
    public function it_can_see_if_branch_is_dirty(): void
    {
        $this->runProcess('touch modified-file.txt');

        $this->assertTrue($this->gitBranch->isDirty());

        $this->runProcess('rm modified-file.txt');

        $this->assertFalse($this->gitBranch->isDirty());
    }

    /**
     * @test
     */
    public function it_can_get_parent_hash_at_pointer_to_master(): void
    {
        $masterCommitId = \trim($this->runProcess("/usr/bin/git log --grep='master commit a' --format='%H'")->getOutput());

        $this->checkoutBranch($this->topicBranchName);

        $this->assertEquals($masterCommitId, $this->gitBranch->getParentHash());
    }

    /**
     * @test
     */
    public function it_can_get_all_changed_files_on_branch_including_uncommitted(): void
    {
        $this->checkoutBranch($this->topicBranchName);

        $changedFiles = $this->gitBranch->getChangedFiles();

        $this->assertCount(2, $changedFiles);

        $this->assertTrue($changedFiles->contains(function (File $file) {
            return $file->getName() === 'test-branch-file-a.txt' && $file->getExtension() === 'txt';
        }));

        $this->assertTrue($changedFiles->contains(function (File $file) {
            return $file->getName() === 'test-branch-file-b.txt' && $file->getExtension() === 'txt';
        }));
    }

    /** @test */
    public function it_gets_the_correct_parent_hash_for_a_branch_using_merge_commit_strategy_on_the_master_branch(): void
    {
        $this->runProcess('/usr/bin/git checkout -b feature-commits-collection');
        $branchName = $this->gitBranch->getName();
        $this->assertEquals('feature-commits-collection', $branchName);
        $command = <<<'EOT'
touch feature-commits-file-a.txt && /usr/bin/git add . && /usr/bin/git commit -m "commit subject a" \ 
-m "Some body message also" && git checkout master && /usr/bin/git merge feature-commits-collection
EOT;
        $this->runProcess($command);
        $masterHash = \trim($this->runProcess('/usr/bin/git rev-parse --verify HEAD')->getOutput());
        $command = <<<'EOT'
/usr/bin/git checkout -b second-branch && touch feature-commits-file-b.txt && /usr/bin/git add . && 
/usr/bin/git commit -m "commit subject b"
EOT;
        $this->runProcess($command);
        $this->assertEquals($masterHash, $this->gitBranch->getParentHash());
    }

    /**
     * @test
     */
    public function it_gets_the_correct_parent_hash_for_a_branch_using_rebase_strategy_on_the_master_branch(): void
    {
        $this->runProcess('/usr/bin/git checkout -b feature-commits-collection');
        $this->runProcess('/usr/bin/git checkout master && git commit -m "Empty commit" --allow-empty');
        $masterHash = \trim($this->runProcess('/usr/bin/git rev-parse --verify HEAD')->getOutput());
        $this->runProcess('/usr/bin/git checkout feature-commits-collection && git rebase master');
        $branchName = $this->gitBranch->getName();
        $this->assertEquals('feature-commits-collection', $branchName);

        $this->assertEquals($masterHash, $this->gitBranch->getParentHash());
    }

    /**
     * @test
     */
    public function it_can_determine_when_a_topic_branch_is_empty(): void
    {
        $this->runProcess('/usr/bin/git checkout -b feature-commits-collection');
        $this->runProcess('/usr/bin/git checkout master && git commit -m "Empty commit" --allow-empty');
        $masterHash = \trim($this->runProcess('/usr/bin/git rev-parse --verify HEAD')->getOutput());

        $this->runProcess('/usr/bin/git checkout feature-commits-collection && git rebase master');

        $branchName = $this->gitBranch->getName();
        $this->assertEquals('feature-commits-collection', $branchName);

        $this->assertTrue($this->gitBranch->isEmpty());
        $this->assertEquals($masterHash, $this->gitBranch->getParentHash());
    }

    /** @test */
    public function it_can_retrieve_a_collection_of_staged_changes_to_be_committed(): void
    {
        $this->runProcess('/usr/bin/git checkout -b feature-commits-collection');
        $command = <<<'EOT'
touch feature-commits-file-a.txt && /usr/bin/git add . && touch feature-commits-file-b.txt
EOT;
        $this->runProcess($command);

        $files = $this->gitBranch->getStagedFiles();

        $this->assertCount(1, $files);

        $this->assertTrue($files->contains(function (File $file) {
            return $file->getName() === 'feature-commits-file-a.txt'
                && $file->getStatus() === 'A';
        }));
    }

    /** @test */
    public function it_can_get_a_collection_of_commits_on_a_topic_branch(): void
    {
        $this->runProcess("/usr/bin/git checkout -b feature-commits-collection");

        $branchName = $this->gitBranch->getName();

        $this->assertEquals("feature-commits-collection", $branchName);
        $command = <<<'EOT'
touch feature-commits-file-a.txt && git add . && git commit -m "commit subject a" -m "Some body message also" && 
echo 'test' > feature-commits-file-a.txt && git add feature-commits-file-a.txt && git commit -m "Commit subject b" && 
touch feature-commits-file-b.txt && git add . && git commit -m "Commit message that is way too long" 
EOT;
        $this->runProcess($command);

        $actualCommitHashes = \explode(\PHP_EOL, $this->runProcess(
            "/usr/bin/git log master..feature-commits-collection --pretty=format:\"%H\""
        )->getOutput());

        $commitsOnBranch = $this->gitBranch->getCommits();

        $this->assertCount(3, $actualCommitHashes);
        $this->assertEquals(3, $commitsOnBranch->count());

        $commits = $commitsOnBranch->all();

        /** @var Commit $commitOne */
        /** @var Commit $commitTwo */
        /** @var Commit $commitThree */
        $commitOne = $commits[0];
        $commitTwo = $commits[1];
        $commitThree = $commits[2];

        $this->assertEquals($actualCommitHashes[2], $commitOne->getHash());
        $this->assertEquals('commit subject a', $commitOne->getCommitMessage()->getSubject());
        $this->assertEquals('Some body message also', $commitOne->getCommitMessage()->getBody());
        $this->assertEquals('Git Review', $commitOne->getAuthor()->getAuthorName());
        $this->assertEquals('testing@git-review.com', $commitOne->getAuthor()->getAuthorEmail());

        $this->assertEquals($actualCommitHashes[1], $commitTwo->getHash());
        $this->assertEquals('Commit subject b', $commitTwo->getCommitMessage()->getSubject());
        $this->assertEquals('', $commitTwo->getCommitMessage()->getBody());
        $this->assertEquals('Git Review', $commitTwo->getAuthor()->getAuthorName());
        $this->assertEquals('testing@git-review.com', $commitTwo->getAuthor()->getAuthorEmail());

        $this->assertEquals($actualCommitHashes[0], $commitThree->getHash());
        $this->assertEquals('Commit message that is way too long', $commitThree->getCommitMessage()->getSubject());
        $this->assertEquals('', $commitThree->getCommitMessage()->getBody());
        $this->assertEquals('Git Review', $commitThree->getAuthor()->getAuthorName());
        $this->assertEquals('testing@git-review.com', $commitThree->getAuthor()->getAuthorEmail());
    }

    private function checkoutBranch($branchName): void
    {
        $this->runProcess("/usr/bin/git checkout ${branchName}");
    }
}
