<?php

namespace Shopworks\Tests\Integration;

use Shopworks\Git\Review\Process\Process;
use Shopworks\Tests\TestCase;

class GitReviewExecutableTest extends TestCase
{
    /** @test */
    public function it_displays_the_command_options_correctly(): void
    {
        $process = new Process("./git-review");
        $process->run();

        $this->assertTrue($process->isSuccessful());

        $processOutput = \trim($process->getOutput());

        $this->assertStringContainsString(
            "  USAGE: git-review <command> [options] [arguments]",
            $processOutput
        );

        $this->assertStringContainsString(
            "  es-lint       Run ESLint on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertStringContainsString(
            "  php-cs-fixer  Run PHP-CS-Fixer on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertStringContainsString(
            "  phpcs         Run PHP Code Sniffer on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertStringContainsString(
            "  phpcbf        Run PHP Code Beautifier on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertStringContainsString(
            "  phpmd         Run PHP Mess Detector on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertStringContainsString(
            "  parallel-lint Run PHP Parallel Lint on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertStringContainsString(
            "  phpstan       Run PHPStan on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );
    }
}
