
Filtering changed files on branch using the following paths:
============================================================

 * assets/
 * fruits/*/assets

Modified files on branch "pineapples"

assets/example1.jsx - added
fruits/oranges/assets/example3.js - added
fruits/pineapples/assets/example2.js - modified

Running command:

node_modules/.bin/eslint --ext js --ext jsx assets/example1.jsx fruits/oranges/assets/example3.js fruits/pineapples/assets/example2.js


 [OK] ESLint checks passed, good job!!!!                                        

