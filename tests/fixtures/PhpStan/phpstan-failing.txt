
Filtering changed files on branch using the following paths:
============================================================

 * app/
 * fruits/*/tests


Running command:

php vendor/bin/phpstan analyse app/ fruits/*/tests -c phpstan.neon -l 7


PHPStan checks failed!
