<?php

namespace Shopworks\Tests\Unit\File;

use PHPUnit\Framework\Assert;
use Shopworks\Git\Review\File\File;
use Shopworks\Git\Review\File\FileCollection;
use Shopworks\Git\Review\VersionControl\DiffStatus;
use Shopworks\Tests\UnitTestCase;

class FileCollectionTest extends UnitTestCase
{
    /** @test */
    public function it_can_create_a_collection_of_files_from_git_diff_output(): void
    {
        $fileCollection = new FileCollection("/tmp");

        $files = [
            new DiffStatus(
                "A",
                "tests/ExampleTest.php"
            ),
            new DiffStatus(
                "M",
                "tests/ExampleTest2.php"
            ),
            new DiffStatus(
                "D",
                "tests/RemoveThisTest.php"
            ),
            new DiffStatus(
                "R",
                "tests/ExampleTestRenamed.php",
                "tests/ExampleTest3.php"
            ),
        ];

        $fileCollection->addFiles($files);

        $actual = $fileCollection->all();

        Assert::assertCount(4, $actual);
        /** @var File $fileOne */
        $fileOne = $actual[0];
        Assert::assertInstanceOf(File::class, $fileOne);
        Assert::assertEquals("tests/ExampleTest.php", $fileOne->getRelativePath());
        Assert::assertEquals("A", $fileOne->getStatus());
        Assert::assertNull($fileOne->getFilePathBeforeRename());

        /** @var File $fileTwo */
        $fileTwo = $actual[1];
        Assert::assertInstanceOf(File::class, $fileTwo);
        Assert::assertEquals("tests/ExampleTest2.php", $fileTwo->getRelativePath());
        Assert::assertEquals("M", $fileTwo->getStatus());
        Assert::assertNull($fileOne->getFilePathBeforeRename());

        /** @var File $fileThree */
        $fileThree = $actual[2];
        Assert::assertInstanceOf(File::class, $fileThree);
        Assert::assertEquals("tests/RemoveThisTest.php", $fileThree->getRelativePath());
        Assert::assertEquals("D", $fileThree->getStatus());

        /** @var File $fileFour */
        $fileFour = $actual[3];
        Assert::assertInstanceOf(File::class, $fileFour);
        Assert::assertEquals("tests/ExampleTestRenamed.php", $fileFour->getRelativePath());
        Assert::assertEquals("R", $fileFour->getStatus());
    }
}
