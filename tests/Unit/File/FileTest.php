<?php

namespace Shopworks\Tests\Unit\File;

use Mockery\MockInterface;
use PhpCsFixer\Tokenizer\Tokens;
use PHPUnit\Framework\Assert;
use Shopworks\Git\Review\File\File;
use Shopworks\Tests\UnitTestCase;

class FileTest extends UnitTestCase
{
    /** @test */
    public function it_can_tell_if_a_file_contains_phpunit_test_classes(): void
    {
        //create partial mock, so we don't have to retrieve a real file contents
        /** @var MockInterface|File $file */
        $file = \Mockery::mock(
            File::class . '[getFileAsTokens]',
            ['A', 'tests/ExampleTest.php', '/tmp/repo-base']
        );

        $file->shouldReceive('getFileAsTokens')->andReturn(
            Tokens::fromCode("<?php class ExampleTest {}")
        );

        Assert::assertTrue($file->containsPhpUnitClass());

        /** @var MockInterface|File $file */
        $file = \Mockery::mock(
            File::class . '[getFileAsTokens]',
            ['A', 'Example.php', '/tmp/repo-base']
        );

        $file->shouldReceive('getFileAsTokens')->andReturn(
            Tokens::fromCode("<?php class Example {}")
        );

        Assert::assertFalse($file->containsPhpUnitClass());
    }
}
