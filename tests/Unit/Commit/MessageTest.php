<?php

namespace Shopworks\Tests\Unit\Commit;

use Shopworks\Git\Review\Commit\Message;
use Shopworks\Tests\UnitTestCase;

class MessageTest extends UnitTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function it_can_retrieve_the_subject_of_a_commit_message(): void
    {
        $message = new Message("Test subject message", "");

        $this->assertEquals("Test subject message", $message->getSubject());
    }

    /** @test */
    public function it_can_retrieve_the_body_of_a_commit_message(): void
    {
        $message = new Message("", "Test body message");

        $this->assertEquals("Test body message", $message->getBody());
    }

    /** @test */
    public function it_can_retrieve_the_subject_length(): void
    {
        $message = new Message("This should be 33 characters long", "");

        $this->assertEquals(33, $message->getSubjectLength());
    }

    /** @test */
    public function it_can_retrieve_the_body_length(): void
    {
        $body = <<<'EOT'
This is a test commit message that doesn't add anything to the project other than being pure rubbish in order
to make these tests pass. The body should be 170 characters.
EOT;
        $message = new Message("", $body);

        $this->assertEquals(170, $message->getBodyLength());
    }
}
