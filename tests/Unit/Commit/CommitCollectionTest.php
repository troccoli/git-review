<?php

namespace Shopworks\Tests\Unit\Commit;

use Illuminate\Support\Collection;
use Shopworks\Git\Review\Commit\Author;
use Shopworks\Git\Review\Commit\Commit;
use Shopworks\Git\Review\Commit\CommitCollection;
use Shopworks\Tests\UnitTestCase;

class CommitCollectionTest extends UnitTestCase
{
    /** @test */
    public function it_can_return_the_commit_count(): void
    {
        $commitCollection = new CommitCollection([
            \Mockery::mock(Commit::class),
            \Mockery::mock(Commit::class),
            \Mockery::mock(Commit::class),
            \Mockery::mock(Commit::class),
        ]);

        $this->assertEquals(4, $commitCollection->count());
    }

    /** @test */
    public function it_can_return_all_of_the_commits_in_the_collection(): void
    {
        $collection = [\Mockery::mock(Commit::class)];
        $commitCollection = new CommitCollection($collection);

        $this->assertEquals($collection, $commitCollection->all());
    }

    /** @test */
    public function it_can_return_the_unique_author_names_in_the_collection(): void
    {
        $collection = [
            \Mockery::mock(Commit::class, [
                'getAuthor' => \Mockery::mock(Author::class, [
                    'getAuthorName' => 'Optimus Prime',
                ]),
            ]),
            \Mockery::mock(Commit::class, [
                'getAuthor' => \Mockery::mock(Author::class, [
                    'getAuthorName' => 'Optimus Prime',
                ]),
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertEquals(new Collection([
            'Optimus Prime',
        ]), $commitCollection->getAuthorNames());

        $collection = [
            \Mockery::mock(Commit::class, [
                'getAuthor' => \Mockery::mock(Author::class, [
                    'getAuthorName' => 'Optimus Prime',
                ]),
            ]),
            \Mockery::mock(Commit::class, [
                'getAuthor' => \Mockery::mock(Author::class, [
                    'getAuthorName' => 'Indianna Jones',
                ]),
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertEquals(new Collection([
            'Optimus Prime',
            'Indianna Jones',
        ]), $commitCollection->getAuthorNames());
    }

    /** @test */
    public function it_can_detect_if_there_are_any_work_in_progress_commits_in_a_collection(): void
    {
        $collection = [
            \Mockery::mock(Commit::class, [
                'isWorkInProgress' => true,
            ]),
            \Mockery::mock(Commit::class, [
                'isWorkInProgress' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertTrue($commitCollection->hasWorkInProgressCommits());

        $collection = [
            \Mockery::mock(Commit::class, [
                'isWorkInProgress' => false,
            ]),
            \Mockery::mock(Commit::class, [
                'isWorkInProgress' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertFalse($commitCollection->hasWorkInProgressCommits());
    }

    /** @test */
    public function it_can_detect_if_there_are_any_fixup_commits_in_a_collection(): void
    {
        $collection = [
            \Mockery::mock(Commit::class, [
                'isFixup' => true,
            ]),
            \Mockery::mock(Commit::class, [
                'isFixup' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertTrue($commitCollection->hasFixupCommits());

        $collection = [
            \Mockery::mock(Commit::class, [
                'isFixup' => false,
            ]),
            \Mockery::mock(Commit::class, [
                'isFixup' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);

        $this->assertFalse($commitCollection->hasFixupCommits());
    }

    /**
     * @dataProvider draftCommitsProvider
     * @test
     */
    public function it_can_detect_if_there_are_any_draft_commits_in_a_collection(bool $isFixUp, bool $isWorkInProgress): void
    {
        $collection = [
            \Mockery::mock(Commit::class, ['isFixup' => $isFixUp, 'isWorkInProgress' => $isWorkInProgress]),
        ];
        $commitCollection = new CommitCollection($collection);
        $this->assertTrue($commitCollection->hasDraftCommits());

        $collection = [
            \Mockery::mock(Commit::class, [
                'isFixup' => false,
                'isWorkInProgress' => false,
            ]),
        ];
        $commitCollection = new CommitCollection($collection);
        $this->assertFalse($commitCollection->hasDraftCommits());
    }

    public function draftCommitsProvider()
    {
        return [
            [false, true],
            [true, false],
            [true, true],
        ];
    }
}
