<?php

namespace Shopworks\Tests\Unit\Process;

use Shopworks\Git\Review\Process\Process;
use Shopworks\Git\Review\Process\ProcessResult;
use Shopworks\Git\Review\Process\ProcessResultCollection;
use Shopworks\Tests\UnitTestCase;

class ProcessResultCollectionTest extends UnitTestCase
{
    /** @test */
    public function it_can_determine_if_collection_has_failed_tasks(): void
    {
        $collection = ProcessResultCollection::make([
            new ProcessResult('command 1', Process::EXIT_CODE_SUCCESS, true),
            new ProcessResult('command 2', Process::EXIT_CODE_FAILED, true),
        ]);

        $this->assertTrue($collection->hasErrors());
    }

    /** @test */
    public function it_can_determine_how_many_failed_tasks_are_in_a_given_process_result_collection(): void
    {
        $collection = ProcessResultCollection::make([
            new ProcessResult('command 1', Process::EXIT_CODE_SUCCESS, true),
            new ProcessResult('command 2', Process::EXIT_CODE_FAILED, true),
            new ProcessResult('command 3', Process::EXIT_CODE_FAILED, true),
            new ProcessResult('command 4', Process::EXIT_CODE_SUCCESS, true),
        ]);

        $this->assertEquals(2, $collection->failCount());
    }

        /** @test */
    public function it_can_determine_how_many_successful_tasks_are_in_a_given_process_result_collection(): void
    {
        $collection = ProcessResultCollection::make([
            new ProcessResult('command 1', Process::EXIT_CODE_SUCCESS, true),
            new ProcessResult('command 2', Process::EXIT_CODE_FAILED, true),
            new ProcessResult('command 3', Process::EXIT_CODE_FAILED, true),
            new ProcessResult('command 4', Process::EXIT_CODE_SUCCESS, true),
            new ProcessResult('command 4', Process::EXIT_CODE_SUCCESS, true),
        ]);

        $this->assertEquals(3, $collection->successfulCount());
    }
}
