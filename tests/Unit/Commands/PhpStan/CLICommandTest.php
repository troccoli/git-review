<?php

namespace Shopworks\Tests\Unit\Commands\PhpStan;

use Shopworks\Git\Review\Commands\PhpStan\CLICommand;
use Shopworks\Tests\UnitTestCase;

class CLICommandTest extends UnitTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function it_can_build_a_command_for_all_config_options(): void
    {
        $command = new CLICommand([
            'bin_path' => 'vendor/bin/phpstan',
            'config_path' => 'phpstan.neon',
            'level' => 7,
        ], ['app/', 'tests/*/examples']);

        $this->assertEquals(
            "php vendor/bin/phpstan analyse app/ tests/*/examples -c phpstan.neon -l 7",
            $command->toString()
        );
    }

    /** @test */
    public function it_adds_the_necessary_defaults_when_no_config_is_provided_but_paths_are(): void
    {
        $command = new CLICommand([], ['app/', 'tests/*/examples']);

        $this->assertEquals(
            "php vendor/bin/phpstan analyse app/ tests/*/examples -c phpstan.neon -l 7",
            $command->toString()
        );
    }
}
