<?php

namespace Shopworks\Tests\Unit\Commands\PhpMessDetector;

use Shopworks\Git\Review\Commands\PhpMessDetector\CLICommand;
use Shopworks\Tests\UnitTestCase;

class CLICommandTest extends UnitTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function it_can_build_a_command_for_all_config_options(): void
    {
        $command = new CLICommand([
            'bin_path' => 'vendor/bin/phpmd',
            'ruleset_file' => 'phpmd.xml',
            'report_format' => 'text',
        ], ['app/', 'tests/*/examples']);

        $this->assertEquals(
            "php vendor/bin/phpmd app/,tests/*/examples text phpmd.xml",
            $command->toString()
        );
    }

    /** @test */
    public function it_adds_the_necessary_defaults_when_no_config_is_provided_but_paths_are(): void
    {
        $command = new CLICommand([], ['app/', 'tests/*/examples']);

        $this->assertEquals("php vendor/bin/phpmd app/,tests/*/examples text phpmd.xml", $command->toString());
    }
}
