<?php

namespace Shopworks\Tests\Unit\VersionControl;

use PHPUnit\Framework\Assert;
use Shopworks\Git\Review\VersionControl\DiffStatus;
use Shopworks\Git\Review\VersionControl\GitDiffParser;
use Shopworks\Tests\UnitTestCase;

class GitDiffParserTest extends UnitTestCase
{
    /** @test */
    public function it_can_parse_diff_status_output_into_a_more_manageable_format(): void
    {
        $parser = new GitDiffParser();

        $expected = [
            new DiffStatus(
                'A',
                'src/NewFile.php'
            ),
            new DiffStatus(
                'M',
                'src/ExistingFile.php'
            ),
            new DiffStatus(
                'D',
                'src/ExistingFile2.php'
            ),
            new DiffStatus(
                'R',
                'tests/ExampleTestRenamed.php',
                'tests/ExampleTest.php'
            ),
            new DiffStatus(
                'R',
                'tests/ExampleTestRenamed2.php',
                'tests/ExampleTest2.php'
            ),
        ];

        Assert::assertEquals(
            $expected,
            $parser->parse(
                \file_get_contents(__DIR__ . '/../../fixtures/VersionControl/git-diff.txt')
            )
        );
    }

    /** @test */
    public function it_handles_when_the_diff_provided_is_empty(): void
    {
        $parser = new GitDiffParser();

        Assert::assertEquals([], $parser->parse(""));
    }
}
