<?php

namespace Shopworks\Git\Review\VersionControl;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class GitDiffParser
{
    public function parse(string $diff): array
    {
        if (empty($diff)) {
            return [];
        }

        return Collection::make($this->splitNewLinesIntoArray(\trim($diff)))
            ->map(function (string $item): DiffStatus {
                $diffStatus = $this->splitTabsIntoArray($item);

                if (\count($diffStatus) === 3 && Str::startsWith($diffStatus[0], "R")) {
                    return new DiffStatus("R", $diffStatus[2], $diffStatus[1]);
                }

                return new DiffStatus($diffStatus[0], $diffStatus[1]);
            })->toArray();
    }

    private function splitNewLinesIntoArray(string $diff): array
    {
        return \explode(\PHP_EOL, $diff);
    }

    private function splitTabsIntoArray(string $diff): array
    {
        return \explode("\t", $diff);
    }
}
