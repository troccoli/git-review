<?php

declare(strict_types=1);

namespace Shopworks\Git\Review;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Shopworks\Git\Review\Commands\CliCommandContract;
use Shopworks\Git\Review\File\File;
use Shopworks\Git\Review\File\GitFilesFinder;
use Shopworks\Git\Review\Process\Process;
use Shopworks\Git\Review\Process\Processor;
use Shopworks\Git\Review\Repositories\ConfigRepository;
use Shopworks\Git\Review\VersionControl\GitBranch;
use Symfony\Component\Console\Input\InputOption;

abstract class BaseCommand extends Command
{
    private const MASTER_BRANCHES = ['master', 'remotes/origin/master'];
    private const EXIT_SUCCESS = 0;
    private const EXIT_FAILURE = 1;
    protected $processor;
    protected $gitFilesFinder;
    protected $configRepository;
    protected $gitBranch;
    protected $process;
    protected $toolName = '';
    protected $realTimeOutput = true;
    protected $config;
    protected $configPaths;

    public function __construct(
        Process $process,
        Processor $processor,
        GitFilesFinder $gitFilesFinder,
        ConfigRepository $configRepository,
        GitBranch $gitBranch
    ) {
        parent::__construct();

        $this->process = $process;
        $this->processor = $processor;
        $this->gitFilesFinder = $gitFilesFinder;
        $this->configRepository = $configRepository;
        $this->gitBranch = $gitBranch;

        $this->setDescription("Run {$this->toolName} on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.");

        $this->addOption(
            "staged-only",
            "so",
            InputOption::VALUE_NONE,
            "Run checks on files staged for commit, only."
        );
    }

    public function handle(): int
    {
        if ($this->configNotFound()) {
            return self::EXIT_FAILURE;
        }

        $ymlConfig = $this->configRepository->get($this->config, []);

        if (!isset($ymlConfig['paths'])) {
            $this->error("No paths have been specified in the config file!");

            return self::EXIT_FAILURE;
        }

        $branchName = $this->gitFilesFinder->getBranchName();

        $isEmpty = !\in_array($branchName, self::MASTER_BRANCHES)
            && $this->gitBranch->isEmpty()
            && !$this->option('staged-only');

        if ($isEmpty) {
            $this->info("This branch is empty, exiting!");

            return self::EXIT_SUCCESS;
        }

        $this->getOutput()->title('Filtering changed files on branch using the following paths:');
        $this->getOutput()->listing($ymlConfig['paths']);

        $paths = $this->resolveFilePaths($ymlConfig, $this->configPaths);

        if (empty($paths)) {
            $this->getOutput()->writeln('No files to scan matching provided filters, nothing to do!');

            return self::EXIT_SUCCESS;
        }

        $command = $this->resolveCommand($ymlConfig, $paths);

        $this->getOutput()->writeln("\n<options=bold,underscore>Running command:</>\n");
        $this->getOutput()->writeln("<info>{$command->toString()}</info>\n");

        $process = $this->runCommand($command);

        if ($process->getExitCode() !== 0) {
            $this->getOutput()->writeln("\n<error>{$this->getErrorMessage()}</error>");

            return self::EXIT_FAILURE;
        }

        $this->getOutput()->newLine();
        $this->getOutput()->success($this->getSuccessMessage());

        return self::EXIT_SUCCESS;
    }

    protected function configNotFound(): bool
    {
        $isEmpty = $this->configRepository->isEmpty();

        if ($isEmpty) {
            $this->getOutput()->error('No `git-review.yml.dist` or `git-review.yml` config file, found!');
        }

        return $isEmpty;
    }

    abstract protected function resolveCommand(array $ymlConfig, array $paths): CliCommandContract;

    protected function runCommand(CliCommandContract $command): Process
    {
        $process = $this->process->simple($command->toString());

        return $this->processor->process($process, $this->realTimeOutput);
    }

    protected function resolveFilePaths(array $ymlConfig, string $config): array
    {
        $branchName = $this->gitFilesFinder->getBranchName();

        if (\in_array($branchName, self::MASTER_BRANCHES)) {
            return $this->configRepository->get($config, []);
        }

        $extensions = $config['extensions'] ?? [];

        /** @var Collection $gitFiles */
        $gitFiles = $this->gitFilesFinder->find(
            $ymlConfig['paths'] ?? [],
            $ymlConfig['exclude_paths'] ?? [],
            $extensions,
            $this->option("staged-only")
        );

        $filePaths = $this->getFilesAsString($gitFiles);

        if ($filePaths->isEmpty()) {
            return [];
        }

        $this->getOutput()->writeln("<options=bold,underscore>Modified files on branch \"${branchName}\"</>\n");

        $this->getOutput()->writeln($gitFiles->map(function (File $file) {
            return $file->getName() . ' - ' . $file->getFormattedStatus();
        })->toArray());

        return $filePaths->toArray();
    }

    protected function getErrorMessage(): string
    {
        return "{$this->toolName} checks failed!";
    }

    protected function getSuccessMessage(): string
    {
        return "{$this->toolName} checks passed, good job!!!!";
    }

    private function getFilesAsString(Collection $files): Collection
    {
        return $files->map(function (File $file) {
            return $file->getRelativePath();
        })->unique();
    }
}
