<?php

namespace Shopworks\Git\Review\File;

use PhpCsFixer\Indicator\PhpUnitTestCaseIndicator;
use PhpCsFixer\Tokenizer\Tokens;
use StaticReview\File\File as StaticReviewFile;

class File extends StaticReviewFile
{
    public function getFileAsTokens(): Tokens
    {
        return Tokens::fromCode(\file_get_contents($this->filePath));
    }

    public function containsPhpUnitClass()
    {
        $testIndicator = new PhpUnitTestCaseIndicator();

        $tokens = \iterator_to_array($testIndicator->findPhpUnitClasses($this->getFileAsTokens()));

        return $this->getExtension() === 'php' && !empty($tokens);
    }
}
