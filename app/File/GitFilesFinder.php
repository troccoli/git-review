<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\File;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Shopworks\Git\Review\VersionControl\GitBranch;
use Spatie\Regex\Regex;

class GitFilesFinder
{
    private $gitBranch;
    private $filesystem;
    private $criteriaFormatter;

    public function __construct(GitBranch $gitBranch, CriteriaFormatter $criteriaFormatter, Filesystem $filesystem)
    {
        $this->gitBranch = $gitBranch;
        $this->filesystem = $filesystem;
        $this->criteriaFormatter = $criteriaFormatter;
    }

    public function find(
        array $includePaths = [],
        array $excludePaths = [],
        array $extensionCriteria = [],
        $stagedOnly = false
    ): Collection {
        $files = $this->getChangedFiles($stagedOnly);

        $filePathCriteria = new FilePathCriteria(
            $this->criteriaFormatter->formatMany($includePaths),
            $this->criteriaFormatter->formatMany($excludePaths)
        );

        return $this->findFilesByGivenCriteria($files, $filePathCriteria, $extensionCriteria);
    }

    public function getBranchName(): string
    {
        return $this->gitBranch->getName();
    }

    private function findFilesByGivenCriteria(
        Collection $files,
        FilePathCriteria $pathCriteria,
        array $extensionCriteria
    ): Collection {
        return $files->filter(function (File $file) use ($pathCriteria) {
            return collect($pathCriteria->getIncludePaths())->contains(function ($criteria) use ($file) {
                return Regex::match($criteria, $file->getRelativePath())->hasMatch();
            });
        })->reject(function (File $file) use ($pathCriteria) {
            return collect($pathCriteria->getExcludePaths())->contains(function ($criteria) use ($file) {
                return Regex::match($criteria, $file->getRelativePath())->hasMatch();
            });
        })->reject(function (File $file) use ($extensionCriteria) {
            $fileNotExists = !$this->filesystem->exists($file->getFullPath());
            $invalidExtension = !empty($extensionCriteria)
                && !\in_array($file->getExtension(), $extensionCriteria, true);

            return $fileNotExists || $invalidExtension;
        })->sort()->values();
    }

    private function getChangedFiles(bool $stagedOnly = false): Collection
    {
        if ($stagedOnly) {
            return $this->gitBranch->getStagedFiles();
        }

        return $this->gitBranch->getChangedFiles();
    }
}
