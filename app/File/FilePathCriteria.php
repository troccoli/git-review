<?php

namespace Shopworks\Git\Review\File;

class FilePathCriteria
{
    private $includePaths;
    private $excludePaths;

    public function __construct(array $includePaths, array $excludePaths)
    {
        $this->includePaths = $includePaths;
        $this->excludePaths = $excludePaths;
    }

    public function getIncludePaths(): array
    {
        return $this->includePaths;
    }

    public function getExcludePaths(): array
    {
        return $this->excludePaths;
    }
}
