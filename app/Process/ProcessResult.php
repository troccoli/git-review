<?php

namespace Shopworks\Git\Review\Process;

class ProcessResult
{
    private $command;
    private $exitCode;
    private $wasSuccessful;

    public function __construct(string $command, int $exitCode, bool $wasSuccessful)
    {
        $this->command = $command;
        $this->exitCode = $exitCode;
        $this->wasSuccessful = $wasSuccessful;
    }

    public function getCommand(): string
    {
        return $this->command;
    }

    public function getExitCode(): int
    {
        return $this->exitCode;
    }

    public function wasSuccessful(): bool
    {
        return $this->wasSuccessful;
    }

    public function getResult(): string
    {
        return "{$this->getCommand()}: " .
            ($this->wasSuccessful() && $this->getExitCode() === 0 ? '<info>✔</info>' : '<error>✘</error>');
    }
}
