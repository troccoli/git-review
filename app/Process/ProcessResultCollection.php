<?php

namespace Shopworks\Git\Review\Process;

use Illuminate\Support\Collection;

class ProcessResultCollection extends Collection
{
    public function hasErrors(): bool
    {
        return $this->failCount() > 0;
    }

    public function successfulCount(): int
    {
        return $this->filter(function (ProcessResult $result) {
            return !$result->wasSuccessful() || $result->wasSuccessful() && $result->getExitCode() === 0;
        })->count();
    }

    public function failCount(): int
    {
        return $this->filter(function (ProcessResult $result) {
            return !$result->wasSuccessful() || $result->wasSuccessful() && $result->getExitCode() !== 0;
        })->count();
    }
}
