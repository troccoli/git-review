<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpCodeSniffer;

use Illuminate\Support\Arr;
use Shopworks\Git\Review\Commands\CliCommandContract;

class CLICommand implements CliCommandContract
{
    private $config;
    private $filePaths;

    public function __construct(array $config, array $filePaths)
    {
        $this->config = $config;
        $this->filePaths = $filePaths;
    }

    public function toString(): string
    {
        $binPath = Arr::get($this->config, 'bin_path', 'vendor/bin/phpcs');

        $command = "php {$binPath}";

        $additionalOptions = $this->resolveAdditionalOptions();

        if (empty($additionalOptions)) {
            return $command;
        }

        return \trim($command . " " . \implode(' ', $additionalOptions));
    }

    private function resolveAdditionalOptions(): array
    {
        $options = collect([
            $this->addFilePaths(),
            $this->showProgress(),
            $this->addVerbosityLevel(),
        ]);

        return $options->reject(function (string $value) {
            return $value === '';
        })->toArray();
    }

    private function addFilePaths(): string
    {
        return \implode(' ', $this->filePaths);
    }

    private function showProgress(): string
    {
        $showProgress = \filter_var(Arr::get($this->config, 'show_progress', true), \FILTER_VALIDATE_BOOLEAN);

        return $showProgress ? '-p' : '';
    }

    private function addVerbosityLevel(): string
    {
        $verbosity = \filter_var(Arr::get($this->config, 'verbosity_level', 0), \FILTER_VALIDATE_INT);

        return $verbosity === 0 ? "" : '-' . \str_repeat('v', $verbosity);
    }
}
