<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\ESLint;

use Shopworks\Git\Review\BaseCommand;
use Shopworks\Git\Review\Commands\CliCommandContract;

class Command extends BaseCommand
{
    protected $signature = 'es-lint';
    protected $toolName = 'ESLint';
    protected $config = 'tools.es_lint';
    protected $configPaths = 'tools.es_lint.paths';

    protected function resolveCommand(array $ymlConfig, array $paths): CliCommandContract
    {
        return new CLICommand($ymlConfig, $paths);
    }
}
