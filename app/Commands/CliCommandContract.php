<?php

namespace Shopworks\Git\Review\Commands;

interface CliCommandContract
{
    public function toString(): string;
}
