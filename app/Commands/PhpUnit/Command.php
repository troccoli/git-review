<?php

namespace Shopworks\Git\Review\Commands\PhpUnit;

use Illuminate\Console\Command as BaseCommand;
use Illuminate\Support\Collection;
use Shopworks\Git\Review\File\File;
use Shopworks\Git\Review\File\GitFilesFinder;
use Shopworks\Git\Review\Process\Process;
use Shopworks\Git\Review\Process\Processor;
use Shopworks\Git\Review\Process\ProcessResult;
use Shopworks\Git\Review\Process\ProcessResultCollection;
use Shopworks\Git\Review\Repositories\ConfigRepository;
use Shopworks\Git\Review\VersionControl\GitBranch;

class Command extends BaseCommand
{
    private const MASTER_BRANCHES = ['master', 'remotes/origin/master'];
    protected $processor;
    protected $gitFilesFinder;
    protected $configRepository;
    protected $gitBranch;
    protected $process;
    protected $signature = 'phpunit {--noResults} {--staged-only} {--list-files}';
    protected $description = 'Run PHPUnit on added/modified test files only!';

    public function __construct(
        Process $process,
        Processor $processor,
        GitFilesFinder $gitFilesFinder,
        ConfigRepository $configRepository,
        GitBranch $gitBranch
    ) {
        parent::__construct();

        $this->process = $process;
        $this->processor = $processor;
        $this->gitFilesFinder = $gitFilesFinder;
        $this->configRepository = $configRepository;
        $this->gitBranch = $gitBranch;
    }

    public function handle(): int
    {
        $branchName = $this->gitFilesFinder->getBranchName();

        if (!$this->option('list-files')) {
            $this->getOutput()->title("Current branch is `{$branchName}`");
            $this->getOutput()->newLine();
        }

        $files = $this->getPossibleTestFilesOnBranch();

        if (!\in_array($branchName, self::MASTER_BRANCHES) && $files->isEmpty()
            && (!$this->option('staged-only') || !$this->option('list-files'))) {
            $this->getOutput()
                ->writeln('There are currently no test files modified or added. Be a champ, add some :-)');

            $this->getOutput()->success("Nothing to do!");

            return PROCESS::EXIT_CODE_SUCCESS;
        }

        if ($this->option('list-files')) {
            $files->each(function (File $file): void {
                $this->getOutput()->writeln($file->getRelativePath());
            });

            return Process::EXIT_CODE_SUCCESS;
        }

        $commands = !\in_array($branchName, self::MASTER_BRANCHES) ? $files->map(function (File $file) {
            return $file->getRelativePath();
        })->toArray() : [];

        $commands = new CLICommand($commands);
        $results = $this->runCommands($commands->getCommands());

        if (!$this->option('noResults')) {
            $this->compileResults($results);
        }

        $this->getOutput()->newLine();

        if ($results->hasErrors()) {
            $this->getOutput()->error(
                "{$results->failCount()} out of {$results->count()} files with PhpUnit tests added or modified " .
                "on this branch, have currently failing tests."
            );

            return Process::EXIT_CODE_FAILED;
        }

        $this->getOutput()->success("{$results->count()} out of {$results->count()} tests files with PhpUnit " .
            "tests added or modified on this branch have passed. Good job!");

        return Process::EXIT_CODE_SUCCESS;
    }

    private function getPossibleTestFilesOnBranch(): Collection
    {
        $changedFiles = ($this->option('staged-only'))
            ? $this->gitBranch->getStagedFiles() : $this->gitBranch->getChangedFiles();

        return $changedFiles->filter(function (File $file) {
            return $file->containsPhpUnitClass();
        })->unique(function (File $file) {
            return $file->getRelativePath();
        });
    }

    private function runCommands(Collection $commands): ProcessResultCollection
    {
        return $commands->reduce(function (ProcessResultCollection $results, string $command) {
            $this->getOutput()->title("Running command: {$command}");
            $process = $this->runCommand($command);

            $results->push(new ProcessResult($command, $process->getExitCode(), $process->isSuccessful()));

            return $results;
        }, ProcessResultCollection::make());
    }

    private function runCommand(string $command): Process
    {
        $process = $this->process->simple($command);

        return $this->processor->process($process, true);
    }

    private function compileResults(ProcessResultCollection $results): void
    {
        $this->getOutput()->newLine();
        $this->getOutput()->title("Results:");
        $this->getOutput()->listing($results->map(function (ProcessResult $result) {
            return $result->getResult();
        })->toArray());
    }
}
