<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpMessDetector;

use Shopworks\Git\Review\BaseCommand;
use Shopworks\Git\Review\Commands\CliCommandContract;

class Command extends BaseCommand
{
    protected $signature = 'phpmd';
    protected $toolName = 'PHP Mess Detector';
    protected $config = 'tools.php_md';
    protected $configPaths = 'tools.php_md.paths';

    protected function resolveCommand(array $ymlConfig, array $paths): CliCommandContract
    {
        return new CLICommand($ymlConfig, $paths);
    }
}
