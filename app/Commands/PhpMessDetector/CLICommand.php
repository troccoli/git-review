<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpMessDetector;

use Illuminate\Support\Arr;
use Shopworks\Git\Review\Commands\CliCommandContract;

class CLICommand implements CliCommandContract
{
    private $config;
    private $filePaths;

    public function __construct(array $config, array $filePaths)
    {
        $this->config = $config;
        $this->filePaths = $filePaths;
    }

    public function toString(): string
    {
        $binPath = Arr::get($this->config, 'bin_path', 'vendor/bin/phpmd');

        $command = "php {$binPath}";

        $additionalOptions = $this->resolveAdditionalOptions();

        if (empty($additionalOptions)) {
            return $command;
        }

        return \trim($command . " " . \implode(' ', $additionalOptions));
    }

    private function resolveAdditionalOptions(): array
    {
        $options = collect([
            $this->addFilePaths(),
            $this->addFormat(),
            $this->addRuleSetFile(),

        ]);

        return $options->reject(function (string $value) {
            return $value === '';
        })->toArray();
    }

    private function addFilePaths(): string
    {
        return \implode(',', $this->filePaths);
    }

    private function addFormat(): string
    {
        return Arr::get($this->config, 'report_format', 'text');
    }

    private function addRuleSetFile(): string
    {
        return Arr::get($this->config, 'ruleset_file', 'phpmd.xml');
    }
}
