<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpStan;

use Shopworks\Git\Review\BaseCommand;
use Shopworks\Git\Review\Commands\CliCommandContract;

class Command extends BaseCommand
{
    protected $signature = 'phpstan';
    protected $toolName = 'PHPStan';
    protected $config = 'tools.php_stan';
    protected $configPaths = 'tools.php_stan.paths';

    protected function resolveCommand(array $ymlConfig, array $paths): CliCommandContract
    {
        return new CLICommand($ymlConfig, $paths);
    }
}
