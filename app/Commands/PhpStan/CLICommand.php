<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpStan;

use Illuminate\Support\Arr;
use Shopworks\Git\Review\Commands\CliCommandContract;

class CLICommand implements CliCommandContract
{
    private $config;
    private $filePaths;

    public function __construct(array $config, array $filePaths)
    {
        $this->config = $config;
        $this->filePaths = $filePaths;
    }

    public function toString(): string
    {
        $binPath = Arr::get($this->config, 'bin_path', 'vendor/bin/phpstan');

        $command = "php {$binPath} analyse";

        $additionalOptions = $this->resolveAdditionalOptions();

        if (empty($additionalOptions)) {
            return $command;
        }

        return \trim($command . " " . \implode(' ', $additionalOptions));
    }

    private function resolveAdditionalOptions(): array
    {
        $options = collect([
            $this->addFilePaths(),
            $this->addConfigPath(),
            $this->addAnalysisLevel(),
        ]);

        return $options->reject(function (string $value) {
            return $value === '';
        })->toArray();
    }

    private function addFilePaths(): string
    {
        return \implode(' ', $this->filePaths);
    }

    private function addConfigPath(): string
    {
        $configPath = Arr::get($this->config, 'config_path', 'phpstan.neon');

        return empty($configPath) ? "" : "-c {$configPath}";
    }

    private function addAnalysisLevel(): string
    {
        $level = \filter_var(Arr::get($this->config, 'analysis_level', 7), \FILTER_VALIDATE_INT);

        return '-l ' . $level;
    }
}
