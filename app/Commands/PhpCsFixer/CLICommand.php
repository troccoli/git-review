<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpCsFixer;

use Illuminate\Support\Arr;
use Shopworks\Git\Review\Commands\CliCommandContract;

class CLICommand implements CliCommandContract
{
    private $config;
    private $filePaths;

    public function __construct(array $config, array $filePaths)
    {
        $this->config = $config;
        $this->filePaths = $filePaths;
    }

    public function toString(): string
    {
        $binPath = Arr::get($this->config, 'bin_path', 'vendor/bin/php-cs-fixer');
        $command = "php {$binPath} fix";

        $additionalOptions = $this->resolveAdditionalOptions();

        if (empty($additionalOptions)) {
            return $command;
        }

        return \trim($command . " " . \implode(' ', $additionalOptions));
    }

    private function resolveAdditionalOptions(): array
    {
        $options = collect([
            $this->addFilePaths(),
            $this->addConfigPath(),
            $this->addDryRunOption(),
            $this->addVerbosityLevel(),
        ]);

        return $options->reject(function (string $value) {
            return $value === '';
        })->toArray();
    }

    private function addFilePaths(): string
    {
        return \implode(' ', $this->filePaths);
    }

    private function addConfigPath(): string
    {
        $configPath = Arr::get($this->config, 'config_path', '');

        return empty($configPath) ? "" : "--config={$configPath}";
    }

    private function addDryRunOption(): string
    {
        $dryRun = \filter_var(Arr::get($this->config, 'dry_run_mode', true), \FILTER_VALIDATE_BOOLEAN);

        return $dryRun ? '--dry-run' : '';
    }

    private function addVerbosityLevel(): string
    {
        $verbosity = (int)Arr::get($this->config, 'verbosity_level', 0);

        return $verbosity === 0 ? "" : '-' . \str_repeat('v', $verbosity);
    }
}
