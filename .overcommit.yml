# Use this file to configure the Overcommit hooks you wish to use. This will
# extend the default configuration defined in:
# https://github.com/brigade/overcommit/blob/master/config/default.yml
#
# At the topmost level of this YAML file is a key representing type of hook
# being run (e.g. pre-commit, commit-msg, etc.). Within each type you can
# customize each hook, such as whether to only run it on certain files (via
# `include`), whether to only display output if it fails (via `quiet`), etc.
#
# For a complete list of hooks, see:
# https://github.com/brigade/overcommit/tree/master/lib/overcommit/hook
#
# For a complete list of options that you can use to customize hooks, see:
# https://github.com/brigade/overcommit#configuration
#
# Uncomment the following lines to make the configuration take effect.

verify_signatures: false

PreCommit:
  ALL:
    problem_on_unmodified_line: warn
    requires_files: true
    required: false
    quiet: false
    exclude:
      - "vendor/*"

  AuthorEmail:
    enabled: true
    description: 'Check author email address is valid'
    requires_files: false
    required: true
    quiet: true
    pattern: '^[^@]+@.*$'

  AuthorName:
    enabled: true
    description: 'Check for author name'
    requires_files: false
    required: true
    quiet: true

  BrokenSymlinks:
    enabled: false

  CaseConflicts:
    enabled: false

  CssLint:
    enabled: false

  EsLint:
    enabled: false

  ForbiddenBranches:
    enabled: true
    description: 'Check for commit to forbidden branch'
    quiet: true
    branch_patterns: ['master']

  JsonSyntax:
    enabled: true
    description: 'Validate JSON syntax'
    required_library: 'json'
    install_command: 'gem install json'
    include: '**/*.json'

  Mdl:
    enabled: false
    description: 'Analyze markdown files with mdl'
    required_executable: 'mdl'
    install_command: 'gem install mdl'
    include: 'docs/*.md'

  MergeConflicts:
    enabled: true
    description: 'Check for merge conflicts'
    quiet: true
    required_executable: 'grep'
    flags: ['-IHn', "^<<<<<<<[ \t]"]

  PhpLint:
    enabled: false

  PhpCs:
    enabled: false

  LineEndings:
    description: 'Check line endings'
    enabled: true
    eol: "\n" # or "\r\n" for Windows-style newlines
    include:
      - '*.php'
      - '*.xml'
      - '*.dist'
      - '*.md'
  XmlLint:
    enabled: true
    description: 'Analyze XML Files with xmllint'
    required_executable: 'xmllint'
    flags: ['--noout']
    include:
      - '*.xml'
      - '*.xml.dist'
      - '*.svg'

  YamlSyntax:
    enabled: true
    description: 'Check YAML syntax'
    required_library: 'yaml'
    include:
      - '.gitlab-ci.yaml'
      - '.overcommit.yml'

  YarnCheck:
    enabled: false

  # Custom commands
  PhpCodeSniffer:
    enabled: true
    description: 'Run PhpCodeSniffer on staged changes'
    required_excecutable: './git-review'
    command: ['./git-review']
    flags: ['phpcs', '--staged-only', '-q']

  PhpCSFixer:
    enabled: true
    description: 'Run PHP-CS-Fixer on staged changes'
    required_excecutable: './git-review'
    command: ['./git-review']
    flags: ['php-cs-fixer', '--staged-only', '-q']

  PhpStan:
    enabled: true
    description: 'Run PHPStan on staged changes'
    required_excecutable: './git-review'
    command: ['./git-review']
    flags: ['phpstan', '--staged-only', '-q']

  PhpParallelLint:
    enabled: true
    description: 'Run PHP Parallel Lint on staged changes'
    required_excecutable: './git-review'
    command: ['./git-review']
    flags: ['parallel-lint', '--staged-only', '-q']
CommitMsg:
  ALL:
    requires_files: false
    quiet: false

  CapitalizedSubject:
    enabled: true
    description: 'Check subject capitalization'

  EmptyMessage:
    enabled: true
    description: 'Check for empty commit message'
    quiet: true

  MessageFormat:
    enabled: true
    description: 'Check commit message matches expected pattern'
    pattern: '(.+)[:\s](.+)'
    expected_pattern_message: '<Module or change type>: <Commit Message Description>'
    sample_message: 'Composer: Update X dependency'

  RussianNovel:
    enabled: false
    description: 'Check length of commit message structure'
    quiet: true

  SingleLineSubject:
    enabled: true
    description: 'Check subject line is a single line'

  SpellCheck:
    enabled: false

  TextWidth:
    enabled: true
    description: 'Check line length of commit subject and body'
    max_subject_width: 60
    min_subject_width: 0
    max_body_width: 72

  TrailingPeriod:
    enabled: true
    description: 'Check for trailing periods in subject'
